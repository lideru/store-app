# story-app

### 项目地址

[点击查看项目网站](http://114.115.144.177:9966/index)
### 运行下面命令安装
```
npm install
```

###  运行下面
```
npm run serve
```

### 运行下面打包
```
npm run build
```

### 自定义配置
See [Configuration Reference](https://cli.vuejs.org/config/).
