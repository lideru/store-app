import {createRouter, createWebHistory} from 'vue-router'
import Index from '../views/Index.vue'

import Home from '@/Home'

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        redirect: "/index",
        children: [
            {
                path: '/index',
                name: 'index',
                component: () => import( '@/views/Index'),
                meta: {title: '首页'}
            }, {
                path: '/ranking',
                name: 'ranking',
                component: () => import( '@/views/Ranking'),
                meta: {title: '排行榜'}
            }, {
                path: '/all',
                name: 'allStory',
                component: () => import( '@/views/AllStory'),
                meta: {title: '全部小说'}
            }, {
                path: '/bookrack',
                name: 'bookrack',
                component: () => import( '@/views/Bookrack'),
                meta: {title: '全部小说'}
            }, {
                path: "/registeredAuthor",
                name: "registeredAuthor",
                meta: {title: '成为作者'},
                component: () => import("@/views/creation/RegisteredAuthor")
            }, {
                path: '/search',
                name: 'search',
                component: () => import( '@/views/Search'),
                meta: {title: '全部小说'}
            }, {
                path: '/bookInformation:id',
                name: 'bookInformation',
                component: () => import( '@/views/BookInformation'),
                meta: {title: '书本信息'}
            }, {
                path: '/browseChapter:id',
                name: 'browseChapter',
                component: () => import( '@/views/BrowseChapter'),
                meta: {title: '疯狂看书中'}
            }, {
                path: '/personalCenter',
                name: 'PersonalCenter',
                component: () => import( '@/views/user/PersonalCenter'),
                meta: {title: '个人中心'},
                redirect: "/user",
                children: [
                    {
                        path: '/user',
                        name: 'user',
                        component: () => import( '@/views/user/User'),
                        meta: {title: '修改用户信息'}
                    },{
                        path: '/browsingHistory',
                        name: 'browsingHistory',
                        component: () => import( '@/views/user/BrowsingHistory'),
                        meta: {title: '浏览历史'}
                    },{
                        path: '/editPassword',
                        name: 'editPassword',
                        component: () => import( '@/views/user/EditPassword'),
                        meta: {title: '修改密码'}
                    },{
                        path: '/favorite',
                        name: 'favorite',
                        component: () => import( '@/views/user/Favorite'),
                        meta: {title: '收藏夹'}
                    },{
                        path: '/userMessage',
                        name: 'userMessage',
                        component: () => import( '@/views/user/UserMessage'),
                        meta: {title: '留言'}
                    },
                ]
            },


            {
                path: '/creation',
                name: 'creation',
                component: () => import( '@/views/creation/Creation'),
                meta: {title: '创作'},
                redirect: "/data",
                children: [

                    {
                        path: "/chapter",
                        name: "chapter",
                        meta: {title: '章节管理'},
                        component: () => import("@/views/creation/Chapter")
                    },
                    {
                        path: "/book",
                        name: "book",
                        meta: {title: '书籍管理'},
                        component: () => import("@/views/creation/Book")
                    },
                    {
                        path: "/data",
                        name: "data",
                        meta: {title: '数据查询'},
                        component: () => import("@/views/creation/Data")
                    }, {
                        path: "/message",
                        name: "message",
                        meta: {title: '用户留言'},
                        component: () => import("@/views/creation/Message")
                    },
                    /*{
                        path: "/comment",
                        name: "comment",
                        meta: {title: '查看评论'},
                        component: () => import("@/views/creation/Comment")
                    },*/
                    {
                        path: "/write",
                        name: "write",
                        meta: {title: '疯狂写作'},
                        component: () => import("@/views/creation/Write")
                    },

                ]
            },

        ]
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import("@/views/Login"),
        meta: {title: '登录'}
    },
    {
        path: '/register',
        name: 'Register',
        component: () => import("@/views/Register"),
        meta: {title: '注册'}
    },

]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
