import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import { Apple } from '@element-plus/icons'
import * as echarts from 'echarts'
import '@/assets/css/global.css'

const app = createApp(App).use(store).use(router).use(ElementPlus).use(require('vue-wechat-title')).mount('#app')

app.echarts = echarts